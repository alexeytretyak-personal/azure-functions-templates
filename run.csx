#r "Microsoft.WindowsAzure.Storage"
#r "Newtonsoft.Json"
using System.Net;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Newtonsoft.Json;


[FunctionName("UploadFileToBlob")]
public static async Task<HttpResponseMessage> Run(HttpRequestMessage req, TraceWriter log)
{
            string accessKey = "";
            string accountName = "";
            string containerName = "";
            string connectionString;
            CloudStorageAccount storageAccount;
            CloudBlobClient client;
            CloudBlobContainer container;
            CloudBlockBlob blob;

             HttpStatusCode returnCode;
             string returnBody;

            dynamic body = await req.Content.ReadAsStringAsync();
            var input_data = JsonConvert.DeserializeObject<UploadedFile>(body as string);

            try
            {
                //uploading blob
                string Author = input_data.Author;
                string FileName = input_data.FileName;
                string DataBase64 = input_data.DataBase64;
                byte[] fileBytes = Convert.FromBase64String(DataBase64);

                //getting BLOB Container
                accountName = System.Environment.GetEnvironmentVariable("StorageAccountName");
                accessKey = System.Environment.GetEnvironmentVariable("AccessKey");
                containerName = System.Environment.GetEnvironmentVariable("storageContainer");
                connectionString = "DefaultEndpointsProtocol=https;AccountName=" + accountName + ";AccountKey=" + accessKey + ";EndpointSuffix=core.windows.net";
                storageAccount = CloudStorageAccount.Parse(connectionString);

                client = storageAccount.CreateCloudBlobClient();

                container = client.GetContainerReference(containerName);

                //Upload File content
                blob = container.GetBlockBlobReference(FileName);

                //create snapshot
                if (blob.Exists()) { 
                    blob.CreateSnapshot();
                }

                blob.Properties.ContentType = "text/plain";
                blob.Metadata["Author"] = Author;
                
                await blob.UploadFromByteArrayAsync(fileBytes,0, fileBytes.Length);

                //return success and URI
                returnCode = HttpStatusCode.OK;
                returnBody = blob.StorageUri.PrimaryUri.AbsoluteUri;


            }
            catch (Exception e)
            {
                // Exception 
                log.Error("Error occured during the opertion: " + e.ToString());
                returnCode = HttpStatusCode.BadRequest;
                returnBody = $"ERROR! " + e;
            }


            return req.CreateResponse(returnCode, returnBody);




}

    public class UploadedFile
    {
        public string Author { get; set; }
        public string FileName { get; set; }
        public string DataBase64 { get; set; }
    }
